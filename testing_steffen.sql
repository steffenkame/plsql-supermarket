-- testing (Steffen)

clear screen; -- clears the dbms outpus
EXECUTE create_backup_tables();

declare v_count number;
begin
  -- count items of products in the beginning
  select count(*) into v_count from products;
  Assert(Case When (v_count = 4) Then 1 Else 0 End, 'wrong assert #004, not 4 items at beginning');
end;
/

declare v_count number;
begin
  -- insert item into products
  INSERT INTO products (product_id, product_name) VALUES (99999, 'Tomato');
  select count(*) into v_count from products where product_name = 'Tomato';
  Assert(Case When (v_count = 1) Then 1 Else 0 End, 'wrong assert #005, insert into products does not work');
end;
/

declare v_count number;
begin
  -- product_buy() - 10 available, buy 0
  update products set amount = 10 where product_name = 'Water';
  select amount into v_count from products where product_name = 'Water';
  product_buy(0, 'Water');
  select amount into v_count from products where product_name = 'Water';
  --dbms_output.put_line(v_count);
  Assert(Case When (v_count = 10) Then 1 Else 0 End, 'wrong assert #006, product_buy() is not working');
end;
/

declare v_count number;
begin
  -- product_buy() - 10 available, buy 1
  update products set amount = 10 where product_name = 'Water';
  select amount into v_count from products where product_name = 'Water';
  product_buy(1, 'Water');
  select amount into v_count from products where product_name = 'Water';
  --dbms_output.put_line(v_count);
  Assert(Case When (v_count = 9) Then 1 Else 0 End, 'wrong assert #007, product_buy() is not working');
end;
/

declare v_count number;
begin
  -- product_buy() - 10 available, buy 10, expect 0
  update products set amount = 10 where product_name = 'Water';
  --select amount into v_count from products where product_name = 'Water';
  product_buy(10, 'Water');
  select amount into v_count from products where product_name = 'Water';
  --dbms_output.put_line(v_count);
  Assert(Case When (v_count = 0) Then 1 Else 0 End, 'wrong assert #008, product_buy() is not working');
end;
/

declare v_count number;
begin
  -- product_buy() - 10 available, try to buy 20, expect to buy only 10
  update products set amount = 10 where product_name = 'Water';
  select amount into v_count from products where product_name = 'Water';
  product_buy(20, 'Water');
  select amount into v_count from products where product_name = 'Water';
  --dbms_output.put_line(v_count);
  Assert(Case When (v_count = 0) Then 1 Else 0 End, 'wrong assert #009, product_buy() is not working');
end;
/

declare v_count number;
begin
  -- product_deliver() + product_buy()
  update products set amount = 10 where product_name = 'Water';
  select amount into v_count from products where product_name = 'Water';
  product_buy(5, 'Water');
  product_deliver(2, 'Water');
  
  select amount into v_count from products where product_name = 'Water';
  --dbms_output.put_line(v_count);
  Assert(Case When (v_count = 7) Then 1 Else 0 End, 'wrong assert #010');
end;
/

EXECUTE restore_tables_from_backup();