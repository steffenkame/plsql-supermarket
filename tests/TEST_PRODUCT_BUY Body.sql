create or replace PACKAGE BODY test_product_buy IS

   --
   -- test product_buy case 1: ...
   --
   PROCEDURE ut_product_buy_one_water IS
   BEGIN
      product_buy(1, CONSTANTS.c_water);
      TEST_HELPERS.get_product_amount_and_assert(CONSTANTS.c_water, CONSTANTS.c_buy_one_water_expected);
   END ut_product_buy_one_water;
   
   PROCEDURE ut_product_buy_111_water IS
   BEGIN
     product_buy(111, CONSTANTS.c_water);
     TEST_HELPERS.get_product_amount_and_assert(CONSTANTS.c_water,CONSTANTS.c_initial_amount_of_water);
   END ut_product_buy_111_water;

  PROCEDURE ut_product_buy_one_milk IS
  BEGIN
    product_buy(1, CONSTANTS.c_milk);
    TEST_HELPERS.get_product_amount_and_assert(CONSTANTS.c_milk, CONSTANTS.c_buy_one_milk_expected);
  END ut_product_buy_one_milk;

  PROCEDURE ut_product_buy_111_milk IS
  BEGIN
    product_buy(111, CONSTANTS.c_milk);
    TEST_HELPERS.get_product_amount_and_assert(CONSTANTS.c_milk,CONSTANTS.c_initial_amount_of_milk);
  END ut_product_buy_111_milk;

  PROCEDURE ut_product_buy_no_water IS
  BEGIN
    product_buy(0, CONSTANTS.c_water);
    TEST_HELPERS.get_product_amount_and_assert(CONSTANTS.c_water,CONSTANTS.c_initial_amount_of_water);
  END ut_product_buy_no_water;

  PROCEDURE ut_product_buy_no_milk IS
  BEGIN
    product_buy(0, CONSTANTS.c_milk);
    TEST_HELPERS.get_product_amount_and_assert(CONSTANTS.c_milk,CONSTANTS.c_initial_amount_of_milk);
  END ut_product_buy_no_milk;

  PROCEDURE setup_product_buy AS
  BEGIN
    TEST_HELPERS.setup_product_amounts();
  END setup_product_buy;

END test_product_buy;