create or replace PACKAGE BODY TEST_MINUTES_TO_DATETIME IS

  PROCEDURE setup_minutes_to_datetime AS
  BEGIN
    NULL;
  END setup_minutes_to_datetime;

  PROCEDURE one_minute_to_datetime AS
    l_expected DATE;
    l_actual DATE;
  BEGIN
    l_expected := TO_DATE('2020-01-01 00:01','yyyy-mm-dd hh24:mi');
    l_actual := minutes_to_datetime(1);
    ut.expect(l_expected).to_equal(l_actual);
  END one_minute_to_datetime;

  PROCEDURE one_day_to_datetime AS
    l_expected DATE;
    l_actual DATE;
  BEGIN
    l_expected := TO_DATE('2020-01-02 00:00', 'yyyy-mm-dd hh24:mi');
    l_actual := minutes_to_datetime(24 * 60);
    ut.expect(l_expected).to_equal(l_actual);
  END one_day_to_datetime;

  PROCEDURE one_week_to_datetime AS
    l_expected DATE;
    l_actual DATE;
  BEGIN
    l_expected := TO_DATE('2020-01-08 00:00', 'yyyy-mm-dd hh24:mi');
    l_actual := minutes_to_datetime(7 * 24 * 60);
    ut.expect(l_expected).to_equal(l_actual);
  END one_week_to_datetime;

  PROCEDURE one_year_to_datetime AS
    l_expected DATE;
    l_actual DATE;
  BEGIN
    l_expected := TO_DATE('2021-01-01 00:00', 'yyyy-mm-dd hh24:mi');
    l_actual := minutes_to_datetime(366 * 24 * 60);
    ut.expect(l_expected).to_equal(l_actual);
    NULL;
  END one_year_to_datetime;

END TEST_MINUTES_TO_DATETIME;