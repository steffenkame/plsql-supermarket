create or replace PACKAGE TEST_MINUTES_TO_DATETIME AS

   --%suite(test_minutes_to_datetime)
   --%suitepath(alltests)
  
   --%beforeeach
   PROCEDURE setup_minutes_to_datetime;
   
   --%test
   PROCEDURE one_minute_to_datetime;
   
   --%test
   PROCEDURE one_day_to_datetime;
   
   --%test
   PROCEDURE one_week_to_datetime;
   
   --%test
   PROCEDURE one_year_to_datetime;
   
   

END TEST_MINUTES_TO_DATETIME;