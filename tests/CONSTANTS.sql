create or replace PACKAGE CONSTANTS AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  
  c_initial_actual CONSTANT NUMBER := 1;
  
  c_initial_amount_of_water CONSTANT NUMBER := 7;
  
  c_initial_amount_of_milk  CONSTANT NUMBER := 8;
  
  c_buy_one_water_expected  CONSTANT NUMBER := 6;
  
  c_buy_one_milk_expected  CONSTANT NUMBER := 7;
  
  c_deliver_111_milk_expected CONSTANT NUMBER := 119;
  
  c_deliver_111_water_expected CONSTANT NUMBER := 118;
  
  c_water CONSTANT VARCHAR2(30) := 'Water';
  
  c_milk  CONSTANT VARCHAR2(30) := 'Milk';

END CONSTANTS;