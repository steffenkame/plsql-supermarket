create or replace NONEDITIONABLE PACKAGE BODY test_product_deliver IS
   
   
   PROCEDURE product_deliver IS     
      l_actual   INTEGER := 0;
      l_expected INTEGER := 0;
      v INTEGER := 0;
   BEGIN
   
      -- populate expected
      SELECT amount into v FROM products WHERE product_name = 'Water';
      l_expected := v + 5;
      
      -- populate actual
      system.product_deliver(5, 'Water');
      SELECT amount into v FROM products WHERE product_name = 'Water';
      l_actual := v;
      
      -- assert
      ut.expect(l_actual).to_equal(l_expected);
      
   END product_deliver;

END test_product_deliver;