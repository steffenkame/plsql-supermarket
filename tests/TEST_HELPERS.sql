create or replace PACKAGE TEST_HELPERS AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  PROCEDURE setup_product_amounts;
  
  PROCEDURE get_product_amount_and_assert (v_product_name IN VARCHAR2,l_expected IN INTEGER);

END TEST_HELPERS;