create or replace PACKAGE BODY TEST_HELPERS AS

  PROCEDURE setup_product_amounts AS
  BEGIN
    update products set amount = CONSTANTS.c_initial_amount_of_water where product_name = CONSTANTS.c_water;
    update products set amount = CONSTANTS.c_initial_amount_of_milk where product_name = CONSTANTS.c_milk;
  END setup_product_amounts;

  PROCEDURE get_product_amount_and_assert (v_product_name IN VARCHAR2, l_expected IN INTEGER) AS
    l_actual INTEGER := 1;
  BEGIN
    select amount into l_actual from products where product_name = v_product_name;
    ut.expect(l_actual).to_equal(l_expected);
  END get_product_amount_and_assert;

END TEST_HELPERS;