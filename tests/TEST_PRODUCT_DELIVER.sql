create or replace PACKAGE test_product_deliver IS

   /* generated by utPLSQL for SQL Developer on 2020-03-24 16:50:59 */

   --%suite(test_product_deliver)
   --%suitepath(alltests)
   
   --%beforeeach
   PROCEDURE setup_product_deliver;
   
   --%test
   PROCEDURE ut_deliver_water;
   
   --%test
   PROCEDURE ut_deliver_milk;
   
   --%test
   PROCEDURE ut_deliver_no_water;
   
   --%test
   PROCEDURE ut_deliver_no_milk;

END test_product_deliver;