create or replace PACKAGE test_product_buy IS

   --%suite(test_product_buy)
   --%suitepath(alltests)
   
   --%beforeeach
   PROCEDURE setup_product_buy;
   
   --%test
   PROCEDURE ut_product_buy_one_water;
   
   --%test
   PROCEDURE ut_product_buy_111_water;
   
   --%test
   PROCEDURE ut_product_buy_one_milk;
   
   --%test
   PROCEDURE ut_product_buy_111_milk;
   
   --%test
   PROCEDURE ut_product_buy_no_water;
   
   --%test
   PROCEDURE ut_product_buy_no_milk;

END test_product_buy;